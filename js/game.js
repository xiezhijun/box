         var x;var y;//记录推车的位置
		 var count=3;//推车所占的原始格子二维数组的值 （一开始为3即为灰色格子）
		 var number=0;//推车下一步到达的位置的二维数组的值
		 var isStart=false;//判断是否开始游戏
		 var sid;//用于清除定时
		 var time=0;//时间
		 var step=-1;//步数
		 var pass=1;//关卡
		 var items;//二维数组(当前关卡)
		 //地图一 
		 var items1=
				[
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,1,1,1,1,1,1,1,0,0,0,0],
				   [0,0,0,0,1,3,3,3,3,3,1,1,1,0,0],
				   [0,0,0,0,1,3,5,4,5,4,3,3,1,0,0],
				   [0,0,0,0,1,3,3,3,3,3,6,3,1,0,0],
				   [0,0,0,0,1,3,5,4,5,4,3,1,1,0,0],
				   [0,0,0,0,1,3,3,3,3,3,3,1,0,0,0],
				   [0,0,0,0,1,1,1,1,1,1,1,1,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				];
		  items=items1.slice();//将第一关的地图复制给当前关卡
		  function initMap(){//初始化地图
				var msg="";
				var flag=false;//用于判断二维数组是否存在5(默认不存在)
				for(var i=0;i<items.length;i++){
					for(var j=0;j<items[i].length;j++){
						if(items[i][j]==6){
							x=i;y=j;
						}
						if(items[i][j]==5){//当存在5的时候
							flag=true;
						}
						msg+="<img  width='51px' height='50px' src='img/"+items[i][j]+".jpg'>";
					}
				}
				document.getElementById("main").innerHTML=msg;//更新img
				updateStep();//更新步数
				isOver();//判断游戏是否出现死锁
				if(!flag&&count!=5){//判断是否过关
					clearInterval(sid);//清除时间
					alert("恭喜你成功过关！你用时"+time+"秒");
					update();//更新第二关地图以及数据
				}
		}
		 //进入下一关的时候更新地图以及时间步数等等数值（地图）
		function update(){
			 isStart=false;//判断是否开始游戏
			 clearInterval(sid);//清除时间定时
		     time=-1;
		     step=-1;//步数
		     pass++;//关卡
		     startTime();//更新时间
		     updatePass();//更新关卡
		     switch(pass){
		     	case 2:
		     	  items=items2.slice();//将第二关的地图复制给当前关卡
		     	  initMap();//再次加载地图
		     	  break;
		     	case 3:
		     	  items=items3.slice();//将第二关的地图复制给当前关卡
		     	  initMap();//再次加载地图
		     	  break;
		     	default:
		     	  alert("恭喜你完成全部关卡");
		     	  location.reload(true);//页面刷新
		     	  break;
		     }   
		}
		function print(){
			alert("游戏结束！你用时"+time+"秒");
			location.reload(true);//页面刷新
		}
		function isOver(){//判断是否出现箱子锁死，存在就提前结束游戏 
		 	if(items[x+1][y]==4){//推车下方存在箱子
		 		if(items[x+2][y]==1){//箱子下方是墙
		 			if(items[x+1][y+1]==1){
		 				print();
		 			}
		 			if(items[x+1][y-1]==1){
		 				print();
		 			}
		 		}
		 	}
		 	if(items[x-1][y]==4){//推车上方存在箱子
		 		if(items[x-2][y]==1){//箱子上方是墙
		 			if(items[x-1][y+1]==1){
		 				print();
		 			}
		 			if(items[x-1][y-1]==1){
		 				print();
		 			}
		 		}
		 	}
		 	if(items[x][y-1]==4){//推车左边存在箱子
		 		if(items[x][y-2]==1){//箱子左边是墙
		 			if(items[x+1][y-1]==1){
		 				print();
		 			}
		 			if(items[x-1][y-1]==1){
		 				print();
		 			}
		 		}
		 	}
		 	if(items[x][y+1]==4){//推车右边存在箱子
		 		if(items[x][y+2]==1){//箱子左边是墙
		 			if(items[x+1][y+1]==1){
		 				print();
		 			}
		 			if(items[x-1][y+1]==1){
		 				print();
		 			}
		 		}
		 	}
		}
		function upMove(){//向上移动
			number=items[x-1][y];
			switch(number){
			    case 3://如果推车的上边是灰色格子
			      items[x-1][y]=6;
			      items[x][y]=count;
			      x--;count=3;
			      initMap(); //位置发生改变，更新地图
			      break;
			    case 5:
			      items[x-1][y]=6;
				  items[x][y]=count;
				  x--;count=5;
				  initMap();
			      break;
			    case 4://如果推车的上边是黄色箱子
			      switch(items[x-2][y]){
			      	case 3:
			      	  items[x-1][y]=6;
			      	  items[x][y]=count;
			      	  items[x-2][y]=4;
			      	  x--;count=3;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x-1][y]=6;
			      	  items[x][y]=count;
			      	  items[x-2][y]=7;
			      	  x--;count=3;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    case 7://如果推车的上边是红色箱子
			      switch(items[x-2][y]){
			      	case 3:
			      	  items[x-1][y]=6;
			      	  items[x][y]=count;
			      	  items[x-2][y]=4;
			      	  x--;count=5;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x-1][y]=6;
			      	  items[x][y]=count;
			      	  items[x-2][y]=7;
			      	  x--;count=5;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    default:
			     break;
			}
		}
		function downMove(){
			number=items[x+1][y];
			switch(number){
			    case 3://如果推车的上边是灰色格子
			      items[x+1][y]=6;
			      items[x][y]=count;
			      x++;count=3;
			      initMap();
			      break;
			    case 5:
			      items[x+1][y]=6;
				  items[x][y]=count;
				  x++;count=5;
				  initMap();
			      break;
			    case 4://如果推车的上边是黄色箱子
			      switch(items[x+2][y]){
			      	case 3:
			      	  items[x+1][y]=6;
			      	  items[x][y]=count;
			      	  items[x+2][y]=4;
			      	  x++;count=3;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x+1][y]=6;
			      	  items[x][y]=count;
			      	  items[x+2][y]=7;
			      	  x++;count=3;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    case 7://如果推车的上边是红色箱子
			      switch(items[x+2][y]){
			      	case 3:
			      	  items[x+1][y]=6;
			      	  items[x][y]=count;
			      	  items[x+2][y]=4;
			      	  x++;count=5;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x+1][y]=6;
			      	  items[x][y]=count;
			      	  items[x+2][y]=7;
			      	  x++;count=5;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    default:
			      break;
			}
		}
		function leftMove(){
			number=items[x][y-1];
			switch(number){
			    case 3://如果推车的上边是灰色格子
			      items[x][y-1]=6;
			      items[x][y]=count;
			      y--;count=3;
			      initMap(); 
			      break;
			    case 5:
			      items[x][y-1]=6;
				  items[x][y]=count;
				  y--;count=5;
				  initMap();
			      break;
			    case 4://如果推车的上边是黄色箱子
			      switch(items[x][y-2]){
			      	case 3:
			      	  items[x][y-1]=6;
			      	  items[x][y]=count;
			      	  items[x][y-2]=4;
			      	  y--;count=3;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x][y-1]=6;
			      	  items[x][y]=count;
			      	  items[x][y-2]=7;
			      	  y--;count=3;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    case 7://如果推车的上边是红色箱子
			      switch(items[x][y-2]){
			      	case 3:
			      	  items[x][y-1]=6;
			      	  items[x][y]=count;
			      	  items[x][y-2]=4;
			      	  y--;count=5;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x][y-1]=6;
			      	  items[x][y]=count;
			      	  items[x][y-2]=7;
			      	  y--;count=5;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    default:
			     break;
			}
		}
		function rightMove(){
			number=items[x][y+1];
			switch(number){
			    case 3://如果推车的上边是灰色格子
			      items[x][y+1]=6;
			      items[x][y]=count;
			      y++;count=3;
			      initMap();
			      break;
			    case 5:
			      items[x][y+1]=6;
				  items[x][y]=count;
				  y++;count=5;
				  initMap();
			      break;
			    case 4://如果推车的上边是黄色箱子
			      switch(items[x][y+2]){
			      	case 3:
			      	  items[x][y+1]=6;
			      	  items[x][y]=count;
			      	  items[x][y+2]=4;
			      	  y++;count=3;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x][y+1]=6;
			      	  items[x][y]=count;
			      	  items[x][y+2]=7;
			      	  y++;count=3;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    case 7://如果推车的上边是红色箱子
			      switch(items[x][y+2]){
			      	case 3:
			      	  items[x][y+1]=6;
			      	  items[x][y]=count;
			      	  items[x][y+2]=4;
			      	  y++;count=5;
			      	  initMap();
			      	  break;
			      	case 5:
			      	  items[x][y+1]=6;
			      	  items[x][y]=count;
			      	  items[x][y+2]=7;
			      	  y++;count=5;
			      	  initMap();
			      	  break;
			      	default:
			      	  break;
			      }
			      break;
			    default:
			     break;
			}
		}
		//判断所按键盘的值来调用上下左右移动的函数
		function key(){//w：87   A：65   D:68   s ：83
			if(isStart){
			 var num=window.event.keyCode;
			 switch(num){
			   case 65:
			     leftMove();
			     break;
			   case 87:
			     upMove();
			     break;
			   case 68 :
			     rightMove();
			     break;
			   case 83:
			     downMove();
			     break;
			   default:
			     break;
			}
		   }	
		}
		//开始游戏和暂停游戏
		function startGame(){
			isStart=isStart==false?true:false;
			if(isStart){
				sid=setInterval("startTime()",1000);
			}
			else{
				clearInterval(sid);
			}
		}
		//更新时间
		function startTime(){
			document.getElementById("time").innerText=++time;
		}
		//更新步数
		function updateStep(){
			document.getElementById("step").innerText=++step;
		}
		//更新关卡
		function updatePass(){
			document.getElementById("pass").innerText="第"+pass+"关";
		}
		function help(){
			alert("wsad按键为上下左右移动按钮\n\n点击开始按钮进行每一关游戏\n\n再次点击开始按钮游戏将暂停");
		}
		// 地图二
		  var items2=
				[
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,1,1,1,1,1,1,1,0,0,0,0],
				   [0,0,0,0,1,5,5,4,5,5,1,0,0,0,0],
				   [0,0,0,0,1,5,5,1,5,5,1,0,0,0,0],
				   [0,0,0,0,1,3,4,4,4,3,1,0,0,0,0],
				   [0,0,0,0,1,3,3,4,3,3,1,0,0,0,0],
				   [0,0,0,0,1,3,4,4,4,3,1,0,0,0,0],
				   [0,0,0,0,1,3,3,1,3,6,1,0,0,0,0],
				   [0,0,0,0,1,1,1,1,1,1,1,0,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				];
		// 地图三
		var items3=
				[
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,1,1,1,1,0,0,1,1,1,1,1,0],
				   [0,0,1,1,3,3,1,0,0,1,3,3,3,1,0],
				   [0,0,1,3,4,3,1,1,1,1,4,3,3,1,0],
				   [0,0,1,3,3,4,5,5,5,5,3,4,3,1,0],
				   [0,0,1,1,3,3,3,3,1,3,6,3,1,1,0],
				   [0,0,0,1,1,1,1,1,1,1,1,1,1,0,0],
				   [0,0,0,0,1,1,1,1,1,1,1,1,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
				   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
			 ];
		
		
	
		